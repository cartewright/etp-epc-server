#include "DataObjectHandler.hpp"
#include "EpcDataProvider.hpp"

using namespace Etp;
using Energistics::Protocol::Core::ProtocolException;

namespace EtpEpc 
{

	void DataObjectHandler::find_children(std::string& uric, std::vector<Resource>& retval)
	{
		std::string  uri = BaseHandler::uri_decode(uric);
		boost::regex rgx(::EtpEpc::Patterns::DataObject);
		
		if(!regex_match(uri, rgx)) {
			ProtocolException err;
			err.m_errorCode = 0; //TODO
			err.m_errorMessage = "Invalid URI for a DataObject";
			throw err;				
		}
		
		auto captures = get_provider()->get_matches(rgx, uri);
		std::string data_space = captures[1];
		std::string obj_type = captures[2];
		std::string uuid = captures[3];
		
		std::stringstream ss;
		ss << obj_type << "_" << uuid << ".xml";
		std::string part_name = ss.str();

		file_ptr f = get_provider()->open(data_space);
		opcPart part=opcPartFind(f->opc(), _X(part_name.c_str()), NULL, 0);

		relation_categories(f->opc(), part, uric, retval);
	}
	
	void DataObjectHandler::relation_categories(opcContainer* c, opcPart part, std::string& uric, std::vector<Resource>& retval)
	{
		std::set<const xmlChar*> relationTypes;
		
		for(opcRelation rel=opcRelationFirst(c, part); OPC_RELATION_INVALID!=rel; rel=opcRelationNext(c, part, rel)) {
			opcPart internal_target=opcRelationGetInternalTarget(c, part, rel);
			const xmlChar *external_target=opcRelationGetExternalTarget(c, part, rel);
			const xmlChar *target=(NULL!=internal_target?internal_target:external_target);
			const xmlChar *prefix=NULL;
			opc_uint32_t counter=-1;
			const xmlChar *type=NULL;
			if (target) {
				opcRelationGetInformation(c, part, rel, &prefix, &counter, &type);    
				relationTypes.insert(type);
			}
		}
		
		for (auto relationType : relationTypes) {
			std::string relString((const char *)relationType);
			relString = relString.substr(relString.find_last_of("/")+1);
			Resource res;
			res.m_uri = uric + "/" +relString;
			res.m_name = relString;
			res.m_resourceType = "Folder";
			res.m_hasChildren = -1;
			res.m_objectNotifiable =  false;
			res.m_channelSubscribable = false;						
			retval.push_back(res);
		}
	}
		
}

