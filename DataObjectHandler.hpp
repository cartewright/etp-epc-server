#ifndef ETPEPC_DATAOBJECTHANDLER_HPP_
#define ETPEPC_DATAOBJECTHANDLER_HPP_

#include "EpcDiscoveryHandler.hpp"

namespace EtpEpc {

	/// Discovery handler for a data object.
	class DataObjectHandler : public EpcDiscoveryHandler
	{
	public:
		DataObjectHandler(epc_provider_ptr provider)
			: EpcDiscoveryHandler(provider)
		{
		}

        virtual ~DataObjectHandler(){}

		virtual void find_children(std::string& uric, std::vector<Resource>& retval) override;
		
		/// Return one resource for each category of relation that exists on this object.
		void relation_categories(opcContainer* c, opcPart part, std::string& uric, std::vector<Resource>& retval);
		
		virtual std::string uri_match() override { return EtpEpc::Patterns::DataObject; }
	};

}

#endif
