#include "DataObjectHandler.hpp"
#include "EpcDataProvider.hpp"
#include "EpcDiscoveryHandler.hpp"
#include "EpcXmlHelper.hpp"
#include "EvilHandler.hpp"
#include "ExternalPartHandler.hpp"
#include "ObjectTypeHandler.hpp"
#include "PathHandler.hpp"
#include "RelationHandler.hpp"
#include "Resqml20Handler.hpp"
#include "RootHandler.hpp"

#include "etp/ContentType.hpp"

using namespace H5;
using namespace Energistics::Datatypes;
using namespace Energistics::Protocol::Core;

namespace EtpEpc 
{

	epc_provider_ptr new_data_provider(std::string path)
	{
		epc_provider_ptr retval(new EpcDataProvider(path));
		
		retval->configure();
		
		return retval;
	}

	EpcDataProvider::EpcDataProvider(const std::string& path) 
		: array_rgx(array_pattern()), _root_path(path) 
	{		
	}

	std::string EpcDataProvider::calc_part_name(DataObject obj) 
	{			
		Etp::ContentType ct(obj.m_resource.m_contentType.c_str());
		std::string retval = "obj_" + ct.type() + "_" + obj.m_resource.m_uuid.get_string() + ".xml";
		return retval;
	}

	std::string EpcDataProvider::calc_part_type(DataObject obj) 
	{			
		std::string retval = obj.m_resource.m_contentType;
		return retval;
	}
		
	void EpcDataProvider::configure()
	{
		epc_provider_ptr me=shared_from_this();
		// Configure the discovery handlers.
		register_discovery_handler(discovery_handler_ptr(new EvilHandler(me)));
		register_discovery_handler(discovery_handler_ptr(new RootHandler(me)));
		register_discovery_handler(discovery_handler_ptr(new Resqml20Handler(me)));
		register_discovery_handler(discovery_handler_ptr(new ObjectTypeHandler(me)));
		register_discovery_handler(discovery_handler_ptr(new DataObjectHandler(me)));
		register_discovery_handler(discovery_handler_ptr(new ExternalPartHandler(me)));
		register_discovery_handler(discovery_handler_ptr(new PathHandler(me)));
		register_discovery_handler(discovery_handler_ptr(new RelationHandler(me)));
	}
	
	ST::ObjectPtr EpcDataProvider::get_object(const std::string& uric) 
	{
		std::string  uri = BaseHandler::uri_decode(uric);
		boost::regex rgx(::EtpEpc::Patterns::DataObject);
		
		if(!regex_match(uri, rgx)) {
			ProtocolException err;
			err.m_errorCode = 0; //TODO
			err.m_errorMessage = "Invalid URI for a DataObject";
			throw err;				
		}
		
		auto captures = get_matches(rgx, uri);
		std::string data_space = captures[1];
		std::string obj_type = captures[2];
		std::string uuid = captures[3];

		std::stringstream ss;
		ss << obj_type << "_" << uuid << ".xml";
		std::string part_name = ss.str();

		ETP_LOG(info) << "get_object(): " << part_name << '\n';

		file_ptr f=open(data_space);
		ST::ObjectPtr retval=ST::ObjectPtr(new ST::Object());
		opcPart part=opcPartFind(f->opc(), _X(part_name.c_str()), NULL, 0);
		if(OPC_PART_INVALID!=part) {
			EpcXmlHelper doc(get_xml_doc(f, part));
			retval->m_dataObject.m_resource.m_uuid.set_string(doc.get_uuid());
			retval->m_dataObject.m_resource.m_name = doc.get_title();
			retval->m_dataObject.m_contentEncoding="";
			doc.write_to_string(retval->m_dataObject.m_data);
		}
		else {
			ProtocolException err;
			err.m_errorCode = 0; //TODO
			std::stringstream os;
			os << "Object with URI " << uri << " not found.";
			err.m_errorMessage = os.str();
			throw err;
		} 
		return retval;
	}

	void EpcDataProvider::delete_object(ST::DeleteObjectPtr message)  
	{
		auto uri = message->m_uri[0];
		file_ptr f=open(uri);
		for(opcPart part=opcPartGetFirst(f->opc());OPC_PART_INVALID!=part;part=opcPartGetNext(f->opc(), part)) {
			if (contains( std::string((const char *)part), uri ) ) {
				opcPartDelete(f->opc(), part);
			}
		}
	}

	AR::DataArrayPtr EpcDataProvider::get_data_array(AR::GetDataArray &request) 
	{
		namespace dt =Energistics::Datatypes;
		std::string uri = BaseHandler::uri_decode(request.m_uri);
		if(!valid_array_uri(uri)) {
			ProtocolException err;
			err.m_errorCode = 0; //TODO
			err.m_errorMessage = "Invalid URI for a DataArray object";
			throw err;
		}
		
		auto captures = get_matches(array_rgx, uri);
		std::string data_space = captures[1];
		std::string external_part_uuid = captures[2];
		std::string path_in_hdf = captures[3];
		
		file_ptr f=open(data_space);
		std::string external_part_name = get_external_part_name(f->opc(), external_part_uuid);


		// Normally should be the same as the data_space, with an extension of .h5, but since it can
		// be specified as anything you want in the rels, we take the parent path of the data space
		// and add back on the file name found in the EPC.
		fs::path hdfname = this->_root_path / ( fs::path(data_space).parent_path() / external_part_name ); 
		H5File* hdfFile = new H5File( hdfname.string().c_str(), H5F_ACC_RDONLY);

		// Open the dataset
		DataSet dataset = hdfFile->openDataSet(path_in_hdf.c_str());

		// Number of dimensions in the dataspace.
		int rank=dataset.getSpace().getSimpleExtentNdims();

		// The ETP DataArray to be returned.
		AR::DataArray* retval=new AR::DataArray();
		
		// Allocate space in our return array
		retval->m_dimensions.resize(rank);

		// Get the size of each dimension. Casting here because hsize_t is unsigned.
		dataset.getSpace().getSimpleExtentDims( (hsize_t *)&retval->m_dimensions[0] );

		retval->m_data.m_item.set_ArrayOfDouble(dt::ArrayOfDouble());
		dt::ArrayOfDouble& arr = retval->m_data.m_item.get_ArrayOfDouble();
		arr.m_values.resize(dataset.getInMemDataSize()/sizeof(double));
		double* dptr = &arr.m_values[0];
		dataset.read(dptr, PredType::NATIVE_DOUBLE); // change PredType::NATIVE_DOUBLE according to the datatype stored in the hdf dataset.
		dataset.close();
		hdfFile->close();
		delete hdfFile;
		
		return AR::DataArrayPtr(retval);
	}
	
	std::string EpcDataProvider::get_external_part_name(opcContainer* opc, const std::string& externalPartUuid) 
	{
		std::string partName = "obj_EpcExternalPartReference_" + externalPartUuid + ".xml";
		opcPart part=opcPartFind(opc, _X(partName.c_str()), NULL, 0);
		auto rel = opcRelationFind(opc, part, _X("Hdf5File"), NULL);
		if(OPC_RELATION_INVALID!=rel) {
			return std::string((const char *)opcRelationGetExternalTarget(opc, _X(partName.c_str()), rel));
		}
		
		return "";
	}

	boost::smatch EpcDataProvider::get_matches(boost::regex rgx, const std::string& uri)
	{
		boost::smatch what;
		boost::regex_search(uri, what, rgx);
		return what;
	}

	xml_doc_ptr EpcDataProvider::get_xml_doc(file_ptr f, opcPart part) 
	{
		return xml_doc_ptr(opcXmlReaderReadDoc(f->opc(), part, 0, 0, 0));
	}

	void EpcDataProvider::put_object(ST::PutObjectPtr data_obj) 
	{
		file_ptr f;
		std::string part_name = calc_part_name(data_obj->m_dataObject);
		std::string part_type = calc_part_type(data_obj->m_dataObject);

		opcPart part=opcPartFind(f->opc(), _X(part_name.c_str()), _X(part_type.c_str()), 0);
		if(OPC_PART_INVALID==part) {
			part=opcPartCreate(f->opc(), _X(part_name.c_str()), _X(part_type.c_str()), 0);
		}
		if(!(OPC_PART_INVALID==part)) {
			opcContainerOutputStream *out=opcContainerCreateOutputStream(f->opc(), part, OPC_COMPRESSIONOPTION_NORMAL);
			if(NULL!=out) {
				opcContainerWriteOutputStream(out, (const opc_uint8_t *)(data_obj->m_dataObject.m_data.c_str()), data_obj->m_dataObject.m_data.size());
				opcContainerCloseOutputStream(out);
			}
		}
	}
	
	fs::path EpcDataProvider::get_root() 
	{
		return _root_path;
	}

	file_ptr EpcDataProvider::open(const std::string& name)
	{
		std::string file_name = ( _root_path / fs::path(name) ).string();
		auto epc_file = new EpcFile(file_name);
		return file_ptr(epc_file);
	}
	
	bool EpcDataProvider::single_file() 
	{
		return fs::is_regular_file(_root_path);
	}
	
	bool EpcDataProvider::valid_array_uri(std::string& uri) 
	{
		return boost::regex_match(uri, array_rgx);
	}
	
	std::string EpcDataProvider::array_pattern() 
	{
		return std::string(R"(^eml://(.*?)/resqml20/obj_EpcExternalPartReference[(]([^)]+)[)](.+)$)");
	}	
}