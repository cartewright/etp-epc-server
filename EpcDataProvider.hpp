#ifndef ETPEPC_EPCDATAPROVIDER_HPP_
#define ETPEPC_EPCDATAPROVIDER_HPP_

#include <boost/enable_shared_from_this.hpp>
#include "etp/DataProvider.hpp"
#include "EtpEpcTypes.hpp"
#include "EpcFile.hpp"

namespace EtpEpc  {
	
	/// Public factory for data provider, enforces post-construction configuration 
	epc_provider_ptr new_data_provider(std::string path);
	
	/// Implements the ETP Data Provider interface for EPC files.
	class EpcDataProvider : public Etp::DataProvider, public boost::enable_shared_from_this<EpcDataProvider> {
	protected:
		EpcDataProvider(const std::string& path); 

		/// Constructor is protected to prevent unconfigured providers.
		EpcDataProvider() 
			: EpcDataProvider(fs::current_path().string())
		{
		}
		
		friend epc_provider_ptr new_data_provider(std::string path);
		
	public:
        
        virtual ~EpcDataProvider(){}

        /// Returns the regular expression for a data array URI
   		std::string array_pattern();	

		/// Services the Store DeleteObject message
		virtual void delete_object(ST::DeleteObjectPtr message) override;

		/// Services the Array GetDataArray message.
		virtual AR::DataArrayPtr get_data_array(AR::GetDataArray &request);
		
		/// Gets the part name for an external target in a container
		std::string get_external_part_name(opcContainer* opc, const std::string& externalPartUuid);
	
		/// Gets all of the capture groups from a regex and uri combination.
        boost::smatch get_matches(boost::regex rgx, const std::string& uri);

		/// Services the Store GetObject message.
		virtual ST::ObjectPtr get_object(const std::string& uric);

		/// Returns the root path we are serving.
		fs::path get_root(); 

		/// Gets the xml document for a given part.
		xml_doc_ptr get_xml_doc(file_ptr f, opcPart part);

		/// Open an EPC file.
		file_ptr open(const std::string& name);
		
		/// Services the Store PutObject message.
		virtual void put_object(ST::PutObjectPtr data_obj);

		/// Returns true if we are serving a single EPC and not a directory.
		bool single_file();
		
		/// Returns true if a URI string matches the expression for a data array
		bool valid_array_uri(std::string& uri);
		
	private:
		/// Calculates an EPC part name for a given data object
		std::string calc_part_name(DataObject obj);

		/// Calculates an EPC content type from the Resource information in a DataObject record.
		std::string calc_part_type(DataObject obj);

		/// Wires up all of the handlers
		void configure();
		
		boost::regex array_rgx;
		
		fs::path _root_path;
	};
}

#endif
