#include "EpcDiscoveryHandler.hpp"

namespace EtpEpc 
{
	
	EpcDiscoveryHandler::EpcDiscoveryHandler(epc_provider_ptr provider)
	{
		_provider = provider;
	}
	
	EpcDiscoveryHandler::~EpcDiscoveryHandler()
	{
	}
	
	epc_provider_ptr EpcDiscoveryHandler::get_provider()
	{
		return _provider;
	}

	Resource EpcDiscoveryHandler::from_data_object(const std::string& objType, const xmlChar * partType, EpcXmlHelper& xml, const std::string& dataSpace) 
	{
		Resource res;
		res.m_uuid.set_string( xml.get_uuid() );

		// citation title
		res.m_name = xml.get_title();
		
		// Uri needs the dataspace, so we come back to the correct file
		res.m_uri = "eml://" + dataSpace + "/resqml20/" + objType + "(" + res.m_uuid.get_string() + ")";
		
		// Content type is exactly the content type in EPC.
		res.m_contentType = (const char *)partType;
		
		// The resource is an XML data object.
		res.m_resourceType = "DataObject";
		
		// default to true, may be over-ridden
		res.m_objectNotifiable = true;
		
		// default to false, may be over-ridden
		res.m_channelSubscribable = false;
		
		// Most objects will have some children, but count is unknown
		res.m_hasChildren = -1;
			
		return res;
	}	
}
