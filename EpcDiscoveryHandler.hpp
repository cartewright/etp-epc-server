#ifndef ETPEPC_EPCDISCOVERYHANDLER_HPP_
#define ETPEPC_EPCDISCOVERYHANDLER_HPP_

#include "etp/DiscoveryHandler.hpp"
#include "EtpEpcTypes.hpp"
#include "EpcXmlHelper.hpp"

namespace EtpEpc 
{
	
/// Base class for all EPC dicovery handlers
class EpcDiscoveryHandler : public Etp::DiscoveryHandler
{
public:
	EpcDiscoveryHandler(epc_provider_ptr provider);
	~EpcDiscoveryHandler();
	
	/// Get a pointer to our data provider.
	epc_provider_ptr get_provider();

	/// Construct a Resource object from basic information about a data object.
	Resource from_data_object(const std::string& objType, const xmlChar * partType, EpcXmlHelper& xml, const std::string& dataSpace = "");

	/// This is the main entry point of a discovery handler. 
	virtual void find_children(std::string& uri, std::vector<Resource>& retval) override = 0;
	
	/// Returns the string form of the regular expression that this handler will match
	virtual std::string uri_match() override = 0;

protected:		  
	epc_provider_ptr _provider; 
};
	
}

#endif
