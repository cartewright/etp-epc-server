#include "EpcFile.hpp"

#include <etp/Etp.hpp>

#include <boost/algorithm/string.hpp>

using namespace boost::algorithm;

namespace EtpEpc  {

EpcFile::EpcFile(std::string name)
	:_name(name),
	_epc(0)
{
	open();
}

EpcFile::~EpcFile()
{
	close();
}

void EpcFile::open()
{
	if(_epc) close();

	_epc = opcContainerOpen( _X(epc_name().c_str()), OPC_OPEN_READ_ONLY, NULL, NULL );
	
	if(!_epc) {
		ETP_LOG(info) << "EpcFile error opening: " << epc_name() << '\n';
		throw "Error opening EPC File ";
	}
	
}

void EpcFile::close()
{
	if (_epc) {
		opcContainerClose(_epc, OPC_CLOSE_NOW);
		_epc=0;
	}
}

opcContainer* EpcFile::opc()
{
	return _epc;
}

std::string EpcFile::epc_name()
{
	if(ends_with(_name, ".epc"))
		return _name;
	return _name + ".epc";
}			

}
