#ifndef EPCFILE_HPP_
#define EPCFILE_HPP_

#include "EtpEpcTypes.hpp"

namespace EtpEpc  {
	
/// Wrapper around the epc container file	
class EpcFile 
{
	public:
		/// Construct for a given file name
		EpcFile(std::string name);

		~EpcFile();

		/// open the file
		void open();

		/// close the file
		void close();

		/// Get the underlying opc container
		opcContainer* opc();

		/// Get the name of the epc file.
		std::string epc_name();

	private:
		std::string _name; 	
		opcContainer* _epc;
		
};

}

#endif
