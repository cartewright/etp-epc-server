#include "EpcXmlHelper.hpp"

namespace EtpEpc {
	
EpcXmlHelper::EpcXmlHelper(xml_doc_ptr doc) :
	_doc(doc),
	_ctx(new_xpath_context())
{

}

std::vector<HdfDataset> EpcXmlHelper::get_external_datasets()
{
	std::vector<HdfDataset> retVal;
	
	auto xpathObj = xmlXPathEvalExpression(_X("//*[eml:PathInHdfFile]"), _ctx.get());
	if (xpathObj != NULL) {
		if (xpathObj->nodesetval && xpathObj->nodesetval->nodeNr>0) {
			for(int i=0; i<xpathObj->nodesetval->nodeNr; i++) {
				auto node = xpathObj->nodesetval->nodeTab[i];
				HdfDataset ds;
				ds.m_dataSetPath = get_one_string(node, "./eml:PathInHdfFile", "");
				ds.m_title = get_one_string(node, ".//eml:Title", "");
				ds.m_uuid = get_one_string(node, ".//eml:UUID", "");
				ds.m_versionString = get_one_string(node, "./eml:VersionString", "");
				retVal.push_back(ds);
			}
		}
	}
	
	return retVal;
}


const char* EpcXmlHelper::get_one_string(const char* xpath, const char* default_value) 
{
	auto xpathObj = xmlXPathEvalExpression(_X(xpath), _ctx.get());
	if(xpathObj != NULL) {
		if(xpathObj->stringval) {
			return (const char *)xpathObj->stringval;
		}
		else if (xpathObj->nodesetval && xpathObj->nodesetval->nodeNr>0) {
			auto node0 = xpathObj->nodesetval->nodeTab[0];
			switch (node0->type) {
				case XML_ATTRIBUTE_NODE:
					return (const char *)node0->children->content;
					break;
				case XML_ELEMENT_NODE:
					return (const char *)node0->children->content;
					break;
				default:
					return "";
			}
		}
	}
	
	return default_value;	
}

const char* EpcXmlHelper::get_one_string(xmlNodePtr context, const char* xpath, const char* default_value) 
{
	_ctx->node=context;
	auto xpathObj = xmlXPathEvalExpression(_X(xpath), _ctx.get());
	if(xpathObj != NULL) {
		if(xpathObj->stringval) {
			return (const char *)xpathObj->stringval;
		}
		else if (xpathObj->nodesetval && xpathObj->nodesetval->nodeNr>0) {
			auto node0 = xpathObj->nodesetval->nodeTab[0];
			switch (node0->type) {
				case XML_ATTRIBUTE_NODE:
					return (const char *)node0->children->content;
					break;
				case XML_ELEMENT_NODE:
					return (const char *)node0->children->content;
					break;
				default:
					return "";
			}
		}
	}
	
	return default_value;	
}

const char* EpcXmlHelper::get_uuid()
{
	return get_one_string("/*/@uuid");
}

const char* EpcXmlHelper::get_title()
{
	return get_one_string("//eml:Citation/eml:Title");
}

xpath_ctx_ptr EpcXmlHelper::new_xpath_context() {
	auto newptr = xmlXPathNewContext(_doc.get());
	xmlXPathRegisterNs(newptr, _X("xsi"), _X("http://www.w3.org/2001/XMLSchema-instance"));
	xmlXPathRegisterNs(newptr, _X("eml"), _X("http://www.energistics.org/energyml/data/commonv2"));
	xmlXPathRegisterNs(newptr, _X("resqml"), _X("http://www.energistics.org/energyml/data/resqmlv2"));
	xmlXPathRegisterNs(newptr, _X("witsml"), _X("http://www.energistics.org/energyml/data/witsmlv2"));
	return xpath_ctx_ptr(newptr);
}

void EpcXmlHelper::write_to_string(std::string& out)
{
	xmlChar *s;
	int size;
	xmlDocDumpMemory(_doc.get(), &s, &size);
	if (s == NULL)
		throw std::bad_alloc();
	try {
		out = (char *)s;
	} catch (...) {
		xmlFree(s);
		throw;
	}
	xmlFree(s);
}


}
