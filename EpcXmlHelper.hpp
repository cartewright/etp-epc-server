#ifndef ETPEPC_EPCXMLHELPER_HPP_
#define ETPEPC_EPCXMLHELPER_HPP_

#include "EtpEpcTypes.hpp"
#include "HdfDataset.hpp"

namespace EtpEpc {
	
	/// Helper class for working with the XML files in an EPC
	class EpcXmlHelper {
		
	public:
		EpcXmlHelper(xml_doc_ptr doc);
		
		/// Get the metadata for all external datasets for an object.
		std::vector<HdfDataset> get_external_datasets();
		
		/// Evaluates an XPath expression that returns exactly one string value.
		const char* get_one_string(const char* xpath, const char* default_value=""); 

		/// Evaluates an XPath expression that returns exactly one string value, relative to a node.
		const char* get_one_string(xmlNodePtr context, const char* xpath, const char* default_value=""); 

		/// Get the uuid of 2.x document
		const char* get_uuid();
		
		/// Get Citation.Title from a 2.x document
		const char* get_title();		
		
		/// Serialize a document to a string.
		void write_to_string(std::string& out);

	private:
		xpath_ctx_ptr new_xpath_context();
			
		xml_doc_ptr _doc;
		xpath_ctx_ptr _ctx;
	};

}

#endif
