#include "EtpEpcTypes.hpp"

namespace EtpEpc 
{
	
	namespace Patterns 
	{
		const std::string DataArray = "^eml://(.*?)/resqml20/obj_EpcExternalPartReference[(]([^)]+)[)](.+)$";
		const std::string DataObject = R"(^eml://(.*?)/resqml20/(obj_\w+)[(](\w{8}-\w{4}-\w{4}-\w{4}-\w{12})[)]$)";
		const std::string PartProxy = R"(^eml://(.*?)/resqml20/(obj_\w+)[(](\w{8}-\w{4}-\w{4}-\w{4}-\w{12})[)]/mlToExternalPartProxy$)";
		const std::string Relation = R"(^eml://(.*?)/resqml20/(obj_\w+)[(](\w{8}-\w{4}-\w{4}-\w{4}-\w{12})[)]/(sourceObject|destinationObject)$)";
	}

	namespace RelationTypes 
	{
		const std::string SourceObject = "sourceObject";
		const std::string DestinationObject = "DestinationObject";
		const std::string ExternalPart = "mlToExternalPartProxy";
	}

	namespace RelationTypeUris 
	{
		const std::string SourceObject = "http://schemas.energistics.org/package/2012/relationships/sourceObject";
		const std::string DestinationObject = "http://schemas.energistics.org/package/2012/relationships/DestinationObject";
		const std::string ExternalPart = "http://schemas.energistics.org/package/2012/relationships/mlToExternalPartProxy";
	}
	

}
