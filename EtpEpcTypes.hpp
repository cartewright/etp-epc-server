#ifndef ETPEPC_ETPEPC_TYPES_HPP_
#define ETPEPC_ETPEPC_TYPES_HPP_

/// Forward types and pointers for library

#include <opc/opc.h>
#include <H5Cpp.h>

// xml library support
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>

namespace EtpEpc 
{

namespace fs = boost::filesystem;

class EpcFile;
class EpcDataProvider;

typedef boost::shared_ptr<EpcFile> file_ptr;
typedef boost::shared_ptr<xmlDoc> xml_doc_ptr;
typedef boost::shared_ptr<xmlXPathContext> xpath_ctx_ptr;
typedef boost::shared_ptr<EpcDataProvider> epc_provider_ptr;

/// String representations of the patterns that match various URI types.
namespace Patterns {
	extern const std::string DataObject;
	extern const std::string DataArray;
	extern const std::string PartProxy;
	extern const std::string Relation;
}

/// Allowable relation types as a 'stub' (ie not the full URI)
namespace RelationTypes {
	extern const std::string ExternalPart;
	extern const std::string SourceObject;
	extern const std::string DestinationObject;
}

/// The full URI of allowable relation types.
namespace RelationTypeUris {
	extern const std::string ExternalPart;
	extern const std::string SourceObject;
	extern const std::string DestinationObject;
}

}


#endif