#ifndef ETPEPC_EVILHANDLER_HPP_
#define ETPEPC_EVILHANDLER_HPP_

#include "EpcDiscoveryHandler.hpp"
#include "boost/filesystem.hpp"

namespace EtpEpc {

	/// Discovery handler to filter potentially malicious URIs. 
	/** Particularly looks for "~" and ".." in URI strings. Adjust as needed.
	 *  Must be registered first in Discovery handler list.
	 */
	class EvilHandler : public EpcDiscoveryHandler 
	{
	public:
		EvilHandler(epc_provider_ptr provider)
			: EpcDiscoveryHandler(provider)
		{
		}

		virtual ~EvilHandler(){}

		virtual void find_children(std::string& uri, std::vector<Resource>& result) override {};		

		virtual std::string uri_match() override { return std::string("^.*([.]{2}|~).*$"); }
	};

}

#endif
