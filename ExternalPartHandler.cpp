#include "ExternalPartHandler.hpp"

#include "EpcDataProvider.hpp"


#include <algorithm>

namespace EtpEpc
{
	void ExternalPartHandler::find_children(std::string& uric, std::vector<Resource>& retval)
	{
		std::string  uri = BaseHandler::uri_decode(uric);
		boost::regex rgx(uri_match());
		
		regex_match(uri, rgx);
		
		auto captures = get_provider()->get_matches(rgx, uri);
		std::string data_space = captures[1];
		std::string obj_type = captures[2];
		std::string uuid = captures[3];
		
		std::stringstream ss;
		ss << obj_type << "_" << uuid << ".xml";
		std::string part_name = ss.str();

		file_ptr f = get_provider()->open(data_space);
		opcContainer* c=f->opc();
		opcPart part=opcPartFind(c, _X(part_name.c_str()), NULL, 0);
		
		auto xml = EpcXmlHelper(get_provider()->get_xml_doc(f, part));
		auto hdfDataSets = xml.get_external_datasets();
		
		for(auto hdfDataSet : hdfDataSets) {
			Resource res;
			res.m_uri = "eml://" + data_space + "/resqml20/objEpcExternalPartReference(" + hdfDataSet.m_uuid + ")" + hdfDataSet.m_dataSetPath ;
			res.m_contentType = "";
			res.m_name = hdfDataSet.m_title;
			retval.push_back(res);
		}

	}
}