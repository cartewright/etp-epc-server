#ifndef ETPEPC_EXTERNALPARTHANDLER_HPP_
#define ETPEPC_EXTERNALPARTHANDLER_HPP_

#include "EpcDiscoveryHandler.hpp"

namespace EtpEpc {

	/// Discovery handler, finds all of the data arrays (i.e. external part references) for a object
	class ExternalPartHandler : public EpcDiscoveryHandler
	{
	public:
		ExternalPartHandler(epc_provider_ptr provider)
			: EpcDiscoveryHandler(provider)
		{
		}

		virtual void find_children(std::string& uric, std::vector<Resource>& retval) override;

		virtual std::string uri_match() override { return EtpEpc::Patterns::PartProxy; }
	};

}

#endif // 
