#ifndef ETPEPC_HDFDATASET_HPP_
#define ETPEPC_HDFDATASET_HPP_

namespace EtpEpc 
{
	/// Structure to hold the information needed to construct a path to an array
	struct HdfDataset {
		std::string m_dataSetPath;
		std::string m_title;
		std::string m_uuid;
		std::string m_versionString;
	};
}

#endif