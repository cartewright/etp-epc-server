#include "ObjectTypeHandler.hpp"
#include "EpcDataProvider.hpp"
#include "etp/ContentType.hpp"
using namespace Etp;

namespace EtpEpc 
{

void ObjectTypeHandler::find_children(std::string& uri, std::vector<Resource>& retval)
{
	auto matches = get_matches(uri);
	
	// Dataspace is the file name
	std::string file_name = matches[1];
	
	// Object type comes immediately after resqml20
	std::string obj_type = matches[2];
	
	
	file_ptr f = _provider->open(file_name.c_str());
	opcContainer* c = f->opc();

	for(opcPart part=opcPartGetFirst(c);OPC_PART_INVALID!=part;part=opcPartGetNext(c, part)) {
		auto partType = opcPartGetType(c, part);
		ContentType ct((const char *)partType);
		if ( ct.type()==obj_type ) {				
			EpcXmlHelper xml(_provider->get_xml_doc(f, part));
			Resource res=from_data_object(obj_type, partType, xml, file_name);		
			retval.push_back(res);
		}
	}
}

}