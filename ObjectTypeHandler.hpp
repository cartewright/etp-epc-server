#ifndef ETPEPC_OBJECTTYPEHANDLER_HPP_
#define ETPEPC_OBJECTTYPEHANDLER_HPP_

#include "EpcDiscoveryHandler.hpp"

namespace EtpEpc {

	/// Discovery hander for a given object type
	class ObjectTypeHandler : public EpcDiscoveryHandler
	{
	public:
		ObjectTypeHandler(epc_provider_ptr provider)
			: EpcDiscoveryHandler(provider)
		{
		}

        virtual ~ObjectTypeHandler(){}

        virtual void find_children(std::string& uri, std::vector<Resource>& retval) override;

		virtual std::string uri_match() override { return std::string("^eml://(.*?)/resqml20/(obj_\\w+)$"); }
	};

}

#endif
