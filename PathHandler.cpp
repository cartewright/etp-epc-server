#include "PathHandler.hpp"
#include "EpcDataProvider.hpp"

using namespace Etp;
using namespace boost::filesystem;

namespace EtpEpc {

	void PathHandler::find_children(std::string& uri, std::vector< Resource >& result)
	{
		handle_path(get_provider()->get_root() / uri.substr(6), result);
	}
	
	Resource PathHandler::from_path(path p) 
	{
		auto path_name = p.string().substr(get_provider()->get_root().string().size());
		if (path_name[0]=='/')
			path_name = path_name.substr(1);
		Resource res;
		res.m_uri = "eml://" + path_name;
		res.m_name = p.stem().string();
		res.m_resourceType = "Folder";
		res.m_hasChildren = -1;
		res.m_objectNotifiable =  false;
		res.m_channelSubscribable = false;						
		return res;
	}
	
	void PathHandler::handle_path(path p, std::vector<Resource>& result)
	{
		try {
			typedef fs::directory_iterator di;
			try {
				if (is_epc(p)) {
					enum_uri_protocols_in_file(p, result);
				}
				else if (exists(p) && is_directory(p)) {
					for (di d = di(p); d!=di(); d++) {
						path candidate = d->path();
						if (exists(candidate) && ( is_directory(candidate) || is_epc(candidate))) {
							result.push_back(from_path(candidate));
						}
					}
				}	
			}
			catch(const fs::filesystem_error& ex) {
				ETP_LOG(error) << ex.what() << '\n';
			}
		}
		catch(const filesystem_error& ex) {
			ETP_LOG(error) << ex.what() << '\n';
		}
	}
	
	void PathHandler::enum_uri_protocols_in_file(path p, std::vector<Resource>& result)
	{
		// TODO - actually check for what is in the file.

		Resource res;
		ETP_LOG(debug) << "Epc Path: " << p << '\n';
		std::string uri;
		
		// If a single file is being served, prevent any other files from being accessed.
		if (get_provider()->single_file())
			uri = "";
		else
			uri= p.string().substr(get_provider()->get_root().string().size()+1);
		
		res.m_uri = "eml://" + uri + "/resqml20";
		res.m_name = "RESQML Earth Model";
		res.m_resourceType = "UriProtocol";
		res.m_hasChildren = -1;
		result.push_back(res);
		
	}
	
	bool PathHandler::is_epc(path p)
	{
		return ( exists(p) && is_regular_file(p) && ( p.extension()==".epc" ) );
	}
}
