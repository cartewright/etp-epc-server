#ifndef ETPEPC_PATHHANDLER_HPP_
#define ETPEPC_PATHHANDLER_HPP_

#include "EpcDiscoveryHandler.hpp"
#include "boost/filesystem.hpp"

namespace EtpEpc {

	/// Discovery handler for anything that is a file or path. 
	/** Handles the dataspace component as a file path relative to the root of 
	 *  the server, and the epc itself as a file.
	 */
	class PathHandler : public EpcDiscoveryHandler 
	{
	public:
		PathHandler(epc_provider_ptr provider)
			: EpcDiscoveryHandler(provider)
		{
		}
		typedef boost::filesystem::path path;

        virtual ~PathHandler(){}

		virtual void find_children(std::string& uri, std::vector<Resource>& result) override;
		
		/// Construct a Resource object from a given path.
		Resource from_path(path p);
		
		/// Expose the basic path handler for other derived classes.
		void handle_path(path p, std::vector<Resource>& result);
		
		/// For an EPC file, return the list of known uri protocols inside it. 
		void enum_uri_protocols_in_file(path p, std::vector<Resource>& result);
		
        /// Determine if a given physical path represents and EPC file.
		bool is_epc(path p);

		virtual std::string uri_match() override { return std::string("^eml://([\\-\\./\\w]*)(/?)$"); }
	};

}

#endif
