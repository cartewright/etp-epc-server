# etp-epc-server #

Demonstration code for using RESQML over ETP. Serves RESQML data from an EPC file as an ETP service.

## Prerequisites ##
1. CMake >= 2.8 (https://cmake.org/download/)
2. Boost libraries >= 1.54 (http://www.boost.org/users/download/)
3. Avro c++ libraries (http://avro.apache.org/releases.html)
4. websocketpp (https://github.com/zaphoyd/websocketpp)
5. etp-cpp-lib library (https://bitbucket.org/energistics/etp-cpp-lib).

## Install and build ##
1. Clone the repository
2. Create a build directory. We recommend out of source builds.
3. Run CMake, adjust as necessary to find boost, etc.
4. Run make

## Running ##

	build$ ./EtpEpcServer 
	[2015-08-18 07:37:02.146929] [0xc2731780] [info]    CWD "/home/etp-epc-server/build"
	[2015-08-18 07:37:02.147057] [0xc2731780] [info]    No data file specified, serving folder: /home/etp-epc-server/build
	[2015-08-18 07:37:02.147624] [0xc2731780] [info]    ETP Server Listening on port 9002.

## Options ##
The following options may be specified. You must choose either --file or --directory, but not both. Default serves current dir.

	build$ ./EtpEpcServer --help
	--help                                This message.
	--port arg (=9002)                    Port to listen on.
	--directory arg (=/home/etp-epc-server/build) Root directory for EPC files.
	--file arg                            Single file to serve.