#include "RelationHandler.hpp"
#include "EpcDataProvider.hpp"
#include "etp/ContentType.hpp"
#include <algorithm>

namespace EtpEpc
{
	void RelationHandler::find_children(std::string& uric, std::vector<Resource>& retval)
	{
		std::string  uri = BaseHandler::uri_decode(uric);
		boost::regex rgx(uri_match());
		
		regex_match(uri, rgx);
		
		auto captures = get_provider()->get_matches(rgx, uri);
		std::string data_space = captures[1];
		std::string obj_type = captures[2];
		std::string uuid = captures[3];
		std::string rel_type = captures[4];
		
		std::stringstream ss;
		ss << obj_type << "_" << uuid << ".xml";
		std::string part_name = ss.str();

		file_ptr f = get_provider()->open(data_space);
		opcContainer* c=f->opc();
		opcPart part=opcPartFind(c, _X(part_name.c_str()), NULL, 0);
				
		for(opcRelation rel=opcRelationFirst(c, part); OPC_RELATION_INVALID!=rel; rel=opcRelationNext(c, part, rel)) {
			opcPart internal_target=opcRelationGetInternalTarget(c, part, rel);
			const xmlChar *external_target=opcRelationGetExternalTarget(c, part, rel);
			const xmlChar *target=(NULL!=internal_target?internal_target:external_target);
			const xmlChar *prefix=NULL;
			opc_uint32_t counter=-1;
			const xmlChar *type=NULL;
			if (target) {
				opcRelationGetInformation(c, part, rel, &prefix, &counter, &type);    
				if(ends_with((char*)type, rel_type)) {
					auto partType = opcPartGetType(c, (opcPart)target);
					ContentType ct((const char *)partType);
					EpcXmlHelper xml(_provider->get_xml_doc(f, (opcPart)target));
					Resource res=from_data_object(ct.type(), partType, xml, data_space);		
					retval.push_back(res);
				}
			}
		}
	}
}