#ifndef ETPEPC_RELATIONHANDLER_HPP_
#define ETPEPC_RELATIONHANDLER_HPP_

#include "EpcDiscoveryHandler.hpp"

namespace EtpEpc {

	/// Discovery handler, finds all of the source and destination
	class RelationHandler : public EpcDiscoveryHandler
	{
	public:
		RelationHandler(epc_provider_ptr provider)
			: EpcDiscoveryHandler(provider)
		{
		}

		virtual void find_children(std::string& uric, std::vector<Resource>& retval) override;


		virtual std::string uri_match() override { return EtpEpc::Patterns::Relation; }

	};

}

#endif // 
