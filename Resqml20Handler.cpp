#include "Resqml20Handler.hpp"
#include "EpcDataProvider.hpp"
#include "etp/ContentType.hpp"

using namespace Etp;

namespace EtpEpc {

Resqml20Handler::Resqml20Handler(epc_provider_ptr provider)
	: EpcDiscoveryHandler(provider)
{
}

void Resqml20Handler::find_children(std::string& uri, std::vector<Resource>& retval)
{
	Resource res;
	
	auto matches = get_matches(uri);
	std::string file_name = matches[1];
				
	file_ptr  f = _provider->open(file_name);
	
	opcContainer* c = f->opc(); 

	for(const xmlChar *type=opcContentTypeFirst(c);NULL!=type;type=opcContentTypeNext(c, type)) {

		ContentType ct((const char*)type);

		if(ct.is_resqml()) {
			res.m_uri = uri + "/" + ct.type() + "";
			res.m_name = ct.type().substr(4);
			res.m_resourceType = "Folder";
			res.m_objectNotifiable =  true;
			res.m_channelSubscribable = false;						
			retval.push_back(res);
		}
	}
}

}
