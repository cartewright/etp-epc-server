#ifndef RESQML20HANDLER_HPP_
#define RESQML20HANDLER_HPP_

#include "EpcDiscoveryHandler.hpp"

namespace EtpEpc {

	/// Discovery Handler for the resqml earth model node. 
	/** Returns a list of 
	  * all the contenttypes that exist in this container. */
	class Resqml20Handler : public EpcDiscoveryHandler 
	{
	public:
		Resqml20Handler(epc_provider_ptr provider);

        virtual ~Resqml20Handler(){}
        
		virtual void find_children(std::string& uri, std::vector<Resource>& retval) override;
		
		virtual std::string uri_match() override { return std::string("^eml://(.*?)/resqml20$"); }
	};

}

#endif
