#include "RootHandler.hpp"
#include "EpcDataProvider.hpp"

using namespace Etp;

namespace fs=boost::filesystem;

namespace EtpEpc {

	RootHandler::RootHandler(epc_provider_ptr provider)
		: PathHandler(provider)
	{
	}
	

	void RootHandler::find_children(std::string& uri, std::vector<Resource>& result)
	{
		handle_path(fs::path(get_provider()->get_root()), result);
	}
}
