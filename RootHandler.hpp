#ifndef ROOTHANDLER_HPP_
#define ROOTHANDLER_HPP_

#include "PathHandler.hpp"

namespace EtpEpc {

	/// Discovery handler for "/"
	class RootHandler : public PathHandler 
	{
	public:
		RootHandler(epc_provider_ptr provider);
        
        virtual ~RootHandler(){}

		virtual void find_children(std::string& uri, std::vector<Resource>& result) override;
		
		virtual std::string uri_match() override { return std::string("/$"); }
	};

}

#endif
