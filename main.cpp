#include <etp/Server.hpp>
#include "etp/Impl/Discovery/StoreImpl.hpp"
#include "etp/Impl/Store/StoreImpl.hpp"

#include <etp/ContentType.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#define ETP_DEFAULT_SERVER_PORT 9002

#define EPC_DECL

#include "EtpEpcTypes.hpp"
#include "EpcFile.hpp"
#include "EpcXmlHelper.hpp"
#include "EpcDataProvider.hpp"
#include "EpcDiscoveryHandler.hpp"
#include "PathHandler.hpp"
#include "RootHandler.hpp"
#include "Resqml20Handler.hpp"
#include "ObjectTypeHandler.hpp"
#include "DataObjectHandler.hpp"
#include "ExternalPartHandler.hpp"
#include <boost/program_options.hpp>

using namespace EtpEpc;
using namespace boost::filesystem;

void conflicting_options(const boost::program_options::variables_map & vm,
                         const std::string & opt1, const std::string & opt2)
{
    if (vm.count(opt1) && !vm[opt1].defaulted() &&
        vm.count(opt2) && !vm[opt2].defaulted())
    {
        throw std::logic_error(std::string("Conflicting options '") +
                               opt1 + "' and '" + opt2 + "'.");
    }
}

int main( int argc, char* argv[] )
{
	if (OPC_ERROR_NONE!=opcInitLibrary()) {
		printf("ERROR: initialization of libopc failed.\n");    
		exit( OPC_ERROR_STREAM );
	}
	
	Etp::Server::Config config;
	std::string _path;
	std::string _data_file;

	try {


		boost::program_options::options_description options;
		options.add_options()
			("help", "This message.")
			("port", po::value<int>(&config.m_port)->default_value(9002), "Port to listen on.")
			("directory", boost::program_options::value<std::string>(&_path)->default_value(current_path().string()), "Root directory for EPC files.")
			("file", boost::program_options::value<std::string>(&_data_file)->default_value(""), "Single file to serve.")
			;

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, options),  vm); // can throw

		/**  --help option  */
		if ( vm.count("help")  )
		{
			std::cout << options;
			return 1;
		}
		
		po::notify(vm);
		
		conflicting_options(vm, "directory", "file");
		
		ETP_LOG(info) << "CWD " << current_path() << '\n';
		
		if(_data_file.size()==0) {
			ETP_LOG(info) << "No data file specified, serving folder: " << _path << '\n';
#if UNIX			
			chdir(_path.c_str());
#endif			
		}
		else {
			if (exists(_data_file)) {
				_path = _data_file;
				ETP_LOG(info) << "Serving data file: " << _data_file << '\n';
			}
			else {
				ETP_LOG(error) << "File " << _data_file << " does not exist.";
				throw file_not_found;
			}
		}

		auto provider = new_data_provider(_path);
		Etp::Server server(config, provider);
		
		server.register_handler_factory(factory< Etp::Impl::ProtocolImpl<Energistics::Protocol::ChannelStreaming::Producer> > );
		server.register_handler_factory(factory<Etp::Impl::Discovery::StoreImpl>);
		server.register_handler_factory(factory<Etp::Impl::Store::StoreImpl>);

		server.main();

		opcFreeLibrary();
	}
	catch(std::string& str) {
		std::cout << "EXCEPTION: " << str << std::endl;
	}
	catch(std::logic_error& err) {
		ETP_LOG(error)  << err.what() << '\n';		
	}
	catch(...) {
		std::cout << "Unknown exception. Exiting." << std::endl;
	}

	return 0;    
}
