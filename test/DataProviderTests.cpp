#include "EtpEpcServerTests.hpp"

TEST_CASE( "Construct" )
{
	auto dp =  new_data_provider(root_folder);
	REQUIRE( dp!=nullptr );
}

TEST_CASE( "Open" )
{
	auto dp = new_data_provider(root_folder);
	dp->open(file_name.c_str());
}

TEST_CASE( "get_hdf_name" )
{
	auto dp = new_data_provider(root_folder);
	file_ptr f = dp->open(file_name);
	std::string hdfPartUuid = "a0f022be-65fc-4dbe-9107-36c55368cfcd";
	std::string rel = dp->get_external_part_name(f->opc(), hdfPartUuid);
	
	REQUIRE(rel=="ALWYN-RESQML.h5");
}

TEST_CASE( "GetArray" )
{
//	 eml://total/ALWYN-RESQML.epc/resqml20 was accepted by N6EtpEpc15Resqml20HandlerE using pattern eml://(.*)/resqml20$

	auto dp = new_data_provider(root_folder);
	dp->open(file_name.c_str());
	da::GetDataArray ga;
	ga.m_uri = "eml://total/ALWYN-RESQML/resqml20/obj_EpcExternalPartReference(a0f022be-65fc-4dbe-9107-36c55368cfcd)/RESQML/07359470-d721-474b-81cd-7886bd4e2f8e/points_patch0";

	REQUIRE(dp->valid_array_uri(ga.m_uri));
    REQUIRE(captures(dp->array_pattern(), ga.m_uri, 1, "total/ALWYN-RESQML"));
    REQUIRE(captures(dp->array_pattern(), ga.m_uri, 2, "a0f022be-65fc-4dbe-9107-36c55368cfcd"));
    REQUIRE(captures(dp->array_pattern(), ga.m_uri, 3, "/RESQML/07359470-d721-474b-81cd-7886bd4e2f8e/points_patch0"));
	

	
	da::DataArrayPtr retval = dp->get_data_array(ga);
	REQUIRE( retval->m_data.m_item.idx()==6 );
	REQUIRE( retval->m_data.m_item.get_ArrayOfDouble().m_values.size() == (4965*3));
	REQUIRE( retval->m_dimensions.size()==2);
	REQUIRE( retval->m_dimensions[0]==4965 );
	REQUIRE( retval->m_dimensions[1]==3 );

}
