#include "EtpEpcServerTests.hpp"

using namespace boost::algorithm;

// Assert that a given handler will accept a given uri
inline bool matches(DiscoveryHandler& h, const char* uri)
{
    return matches(h.uri_match(), uri);
}

// Assert that the captured value in a given position of a uri is the expected value
inline bool captures(Etp::DiscoveryHandler& h, const char* uri, int position, const char* expected)
{
    return captures(h.uri_match(), uri, position, expected);
}


TEST_CASE("RootHandler")
{
	auto dp =  new_data_provider(root_folder);
	auto rh = RootHandler(dp);
	
	REQUIRE(matches(rh, "/"));
	
	rv results;
	std::string uri = "/";
	rh.find_children(uri, results);
	
	REQUIRE(results.size() > 0);
	
	for(auto res : results) {
		REQUIRE(starts_with(res.m_uri, "eml://"));
	}
}

TEST_CASE("RootHandlerSingleFile")
{
	
	auto dp =  new_data_provider(root_folder + "/" + file_name + ".epc");
	auto rh = RootHandler(dp);
	
	rv results;
	std::string uri = "/";
	rh.find_children(uri, results);
	
	// When a a single file is served, then there should be exactly
	// one URI returned, which is the resqml20 root of the file
	// (Assuming our test EPC has only resqml data)
	REQUIRE(results.size() == 1);	
	REQUIRE(starts_with(results[0].m_uri, "eml:///resqml20"));
}

TEST_CASE("PathHandler")
{
	auto dp =  new_data_provider(root_folder);
	auto ph = PathHandler(dp);
	
	REQUIRE(matches(ph, "eml://foo/bar"));
	REQUIRE(matches(ph, "eml://"));
	
	// Trailing slashes fail
	REQUIRE(!captures(ph, "eml://a/b/c/", 1, "a/b/c"));
	REQUIRE(captures(ph, "eml://a/b/c", 1, "a/b/c"));
	
	Resource resource = ph.from_path(dp->get_root());
	REQUIRE(resource.m_uri == "eml://");
	
	resource = ph.from_path(dp->get_root() / "total");
	REQUIRE(resource.m_uri == "eml://total");
}

TEST_CASE("ObjectTypeHandler")
{
	auto dp =  new_data_provider(root_folder);
	auto fth = ObjectTypeHandler(dp);

	char uri[] = "eml://a/b/c/resqml20/obj_FaultInterpretation";
	
	REQUIRE(matches(fth, uri));
	REQUIRE(captures(fth, uri, 1, "a/b/c"));
	REQUIRE(captures(fth, uri, 2, "obj_FaultInterpretation"));
	
	char uri1[] = "eml:///resqml20/obj_FaultInterpretation";
	// Should match with default data space
	REQUIRE(matches(fth, uri1));
	REQUIRE(captures(fth, uri1, 1, nullptr));
	REQUIRE(captures(fth, uri1, 2, "obj_FaultInterpretation"));
	
}

TEST_CASE("DataObjectHandler")
{
	auto dp =  new_data_provider(root_folder);
	auto doh = DataObjectHandler(dp);
	
	char uri[] = "eml://a/b/c/resqml20/obj_FaultInterpretation(4b682232-1476-40f3-a142-4e250e431ca4)";
	
	REQUIRE(matches(doh, uri));
	REQUIRE(captures(doh, uri, 1, "a/b/c"));
	REQUIRE(captures(doh, uri, 2, "obj_FaultInterpretation"));
	REQUIRE(captures(doh, uri, 3, "4b682232-1476-40f3-a142-4e250e431ca4"));
	
	std::string uriString = "eml://total/ALWYN-RESQML/resqml20/obj_PointSetRepresentation(055efaa6-4541-4c1e-9bab-cc2475992836)";
	
	rv results;
	doh.find_children(uriString, results);
}
