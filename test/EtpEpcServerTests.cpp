#define CATCH_CONFIG_MAIN
#include "EtpEpcServerTests.hpp"

using namespace EtpEpc;
using namespace Energistics::Protocol::Discovery;
using namespace boost::filesystem;


std::string root_folder = "./data";
std::string file_name = "total/ALWYN-RESQML";

TEST_CASE("Sample Data Exists")
{
	path p( "." );
	REQUIRE( exists(p) );
	
	p /= "data";
	REQUIRE( exists(p) );
	
	p /= "total/ALWYN-RESQML.epc";
	REQUIRE( exists(p) );
}

