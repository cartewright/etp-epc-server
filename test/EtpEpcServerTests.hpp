#include "catch.hpp"
#include "EpcDataProvider.hpp"
#include "EpcXmlHelper.hpp"
#include "EpcDataProvider.hpp"
#include "EpcDiscoveryHandler.hpp"
#include "PathHandler.hpp"
#include "RootHandler.hpp"
#include "Resqml20Handler.hpp"
#include "ObjectTypeHandler.hpp"
#include "DataObjectHandler.hpp"
#include "ExternalPartHandler.hpp"

#include <boost/algorithm/string.hpp>

using namespace EtpEpc;
using namespace Energistics::Protocol::Discovery;
using namespace boost::filesystem;

namespace da =Energistics::Protocol::DataArray;

typedef std::vector<Resource> rv;

extern std::string root_folder;
extern std::string file_name;

// Assert that a given handler will accept a given uri
inline bool matches(const std::string& expr, const std::string& uri)
{
	boost::regex rgx(expr);
	boost::smatch what;
	return boost::regex_match(std::string(uri), what, rgx, boost::match_default);
}

// Assert that the captured value in a given position of a uri is the expected value
inline bool captures(const std::string& expr, const std::string& uri, int position, const char* expected)
{
	boost::regex rgx(expr);
	boost::smatch what;
	if (boost::regex_match(std::string(uri), what, rgx, boost::match_default)) {
		if (expected==nullptr)
			return (what[position].length() == 0);
		return what[position] == expected;
	}
	
	return false;
}


